//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

public class StringTrie: Trie<String> {
    
    /// store the string
    public func putString(_ string: String) {
        putTrie(string.pathIterator).value = string
    }
    
    /// return the string if it is stored, nil otherwise
    public func getString(_ string: String) -> String? {
        return getTrie(string.pathIterator)?.value
    }
    
    /// remove string specified (replace with nil
    public func removeString(_ string: String) {
        getTrie(string.pathIterator)?.value = nil
    }
    
    /// remove all strings beginning with the specified string
    public func removeStringsBeginningWith(_ string: String) {
        getTrie(string.pathIterator)?.remove()
    }
    
}


