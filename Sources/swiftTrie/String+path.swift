//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

public extension String {
    public var pathIterator: PathIterator {
        return StringPathIterator(self)
    }
}

public class StringPathIterator: PathIterator {
    public let string: String
    private var iterator: String.UTF8View.Iterator
    
    public init(_ string: String) {
        self.string = string
        self.iterator = self.string.utf8.makeIterator()
    }
    
    public func next() -> Int? {
        if let i = self.iterator.next() {
            return Int(i)
        }
        return nil
    }
}
