//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

/// Trie
/// ====
/// A trie is part of an index tree structure that can be navigated efficiently by a sequence of integers.
/// Any Trie can yield an iterator to traverse its children in logical recursive descent order.
///
/// Each Trie can carry a value of type T supplied by the user, and it is up to the user to define the mapping
/// between the value of type T and the index path of the trie.  A common case might be where the characters of a string
/// are used as the indices, thereby forming a fast lookup for a mapping string to type T.  This would be equivalent to
/// [String: T], but traversable by ordered String keys.
///
public class Trie<T> : Sequence {
    // Design:
    // Each Trie has zero or more children of type Trie<T> arranged in a unidirectional linked list,
    // and each child has a back-pointer to its parent.  This forms the 'tree-like' backbone of the
    // structure.
    // Each Trie knows its 'path position' in its parent.  This is the value of the path iterator that represents
    // this node in the tree.
    // Each Trie may contain a sequence of path common to all its children.  
    
    public private(set) var compact: [Int]? = nil
    public private(set) weak var parent: Trie<T>? = nil
    public private(set) var nextSibling: Trie<T>? = nil
    public private(set) var firstChild: Trie<T>? = nil
    public let indexInParent: Int
    public var value: T? = nil
    
    public init() {
        self.parent = nil
        self.indexInParent = -1
    }
    
    private init(parent: Trie<T>?, indexInParent: Int, pathIterator: PathIterator) {
        self.parent = parent
        self.indexInParent = indexInParent
        var compact = [Int]()
        while let i = pathIterator.next() {
            compact.append(i)
        }
        if !compact.isEmpty {
            self.compact = compact
        }
    }
    
    /// Return the Trie at the index path defined by a pathIterator.  If no Trie exists at the specified path then return nil.
    ///
    /// Note that the index path is relative to the receiver, so calling this function on different tries in the same tree
    /// will yield different results.  Unless you understand this, always call this method on the root of your tree.
    public func getTrie(_ pathIterator: PathIterator) -> Trie<T>? {
        guard let index = pathIterator.next() else {
            // no more path so must be us
            return self
        }
        // iterate through compact and path until they diverge
        var pathIndex: Int? = index
        if let compact = self.compact {
            let compactIterator = IntegerPathIterator(compact)
            var compactIndex = compactIterator.next()
            while pathIndex == compactIndex {
                if nil == pathIndex {
                    // both ran out together - it must be me
                    return self
                }
                pathIndex = pathIterator.next()
                compactIndex = compactIterator.next()
            }
            if nil == pathIndex {
                // ran out of path before compact - we do not have the element
                return nil
            } else if nil == compactIndex {
                // ran out of compact before path
            }
        }
        
        if let pathIndex = pathIndex {
            if nil == self.firstChild {
                return nil
            }
            var child = self.firstChild
            while let ch = child {
                if pathIndex == ch.indexInParent {
                    return ch.getTrie(pathIterator)
                }
                if pathIndex < ch.indexInParent {
                    return nil
                }
                child = ch.nextSibling
            }
        }
        return nil
    }
    
    /// Return the Trie at the index path defined by a pathIterator.  If no Trie exists at the specified path then one is created.
    ///
    /// Note that the path is relative to the receiver, so calling this function on different tries in the same tree
    /// will yield different results.  Unless you understand this, always call this method on the root of your tree.
    public func putTrie(_ pathIterator: PathIterator) -> Trie<T> {
        // iterate through compact and path until they diverge
        if let index = pathIterator.next() {
            var pathIndex: Int? = index
            if let compact = self.compact {
                let compactIterator = IntegerPathIterator(compact)
                var compactIndex = compactIterator.next()
                var newCompact = [Int]()
                while nil != pathIndex && nil != compactIndex && pathIndex == compactIndex  {
                    newCompact.append(compactIndex!)
                    pathIndex = pathIterator.next()
                    compactIndex = compactIterator.next()
                }
                
                if nil == pathIndex && nil == compactIndex {
                    // path and compact ran out together so we are the trie
                    return self;
                } else if let compactIdx = compactIndex {
                    // Either we have run out of path before the end of the compact list, or the path and the compact list have diverged.
                    // In either case we need to split the compact list and then add a Trie for the remainder of the compact list.
//                    let oldChildren = self.children
//
//                    self.compact = newCompact.isEmpty ? nil : newCompact
//                    self.children = [Trie<T>?]()
//                    self.ensureChildCapacity(capacity: compactIdx)
//
//                    let newTrie = Trie<T>(parent: self, indexInParent: compactIdx, pathIterator: compactIterator)
//                    newTrie.children = oldChildren
//                    if let oldChildren = oldChildren {
//                        for c in oldChildren {
//                            if let child = c {
//                                child.parent = newTrie
//                            }
//                        }
//                    }
//                    newTrie.value = self.value
//                    self.value = nil
//                    self.children?[compactIdx] = newTrie
                    self.compact = newCompact.isEmpty ? nil : newCompact
                    let newTrie = Trie(parent: self, indexInParent: compactIdx, pathIterator: compactIterator)
                    newTrie.firstChild = self.firstChild
                    var ch = newTrie.firstChild
                    while nil != ch {
                        ch?.parent = newTrie
                        ch = ch?.nextSibling
                    }
                    newTrie.value = self.value
                    self.value = nil
                    self.firstChild = newTrie
                }
            }
            
            if let pathIdx = pathIndex {
                guard let firstChild = self.firstChild else {
                    let newChild = Trie<T>(parent: self, indexInParent: pathIdx, pathIterator: pathIterator)
                    self.firstChild = newChild
                    return newChild.putTrie(pathIterator)
                }
                if firstChild.indexInParent > pathIdx {
                    let newChild = Trie<T>(parent: self, indexInParent: pathIdx, pathIterator: pathIterator)
                    newChild.nextSibling = firstChild
                    self.firstChild = newChild
                    return newChild.putTrie(pathIterator)
                }
                
                var child = self.firstChild
                while nil != child {
                    if pathIdx == child?.indexInParent {
                        return child!.putTrie(pathIterator)
                    }
                    if let next = child?.nextSibling {
                        if next.indexInParent > pathIdx {
                            let newChild = Trie<T>(parent: self, indexInParent: pathIdx, pathIterator: pathIterator)
                            newChild.nextSibling = next
                            child?.nextSibling = newChild
                            return newChild.putTrie(pathIterator)
                        }
                    } else {
                        let newChild = Trie<T>(parent: self, indexInParent: pathIdx, pathIterator: pathIterator)
                        child?.nextSibling = newChild
                        return newChild.putTrie(pathIterator)
                    }
                    child = child?.nextSibling
                }
            }
        }
        return self
    }
    
    public final func makeIterator() -> TrieIterator<T> {
        return TrieIterator<T>(self)
    }
    
    /// Remove the receiver from its parent. This has no effect on the root trie
    public final func remove() {
        if self.parent?.firstChild === self {
            self.parent?.firstChild = self.nextSibling
        } else {
            var look = self.parent?.firstChild
            while nil != look {
                if look?.nextSibling === self {
                    look?.nextSibling = self.nextSibling
                    return
                }
                look = look?.nextSibling
            }
        }
    }
    
    /// Return the next Trie in recursive descent order.  If there is no logical next then return nil.
    ///
    /// Note that this iteration will not stop until the end of the entire tree, even if started on a Trie
    /// which is not the root.  To iterate only the subtree of a Trie use the standard swift iterator idiom
    public final func nextTrie() -> Trie<T>? {
//        if let child = self.children?.first(where: {$0 != nil}) {
//            return child
//        }
//        if let parent = self.parent {
//            return parent.childAfter(self.indexInParent)
//        }
//        return nil
        if let child = self.firstChild {
            return child
        }
        if let sibling = self.nextSibling {
            return sibling
        }
        return parent?.nextUp
    }
    
    private var nextUp: Trie? {
        return self.nextSibling ?? self.parent?.nextUp
    }
}

private extension Trie {

//    private func ensureChildCapacity(capacity: Int) {
//        var childCopy = self.children ?? [Trie<T>?]()
//        childCopy.reserveCapacity(capacity)
//        while childCopy.count <= capacity {
//            childCopy.append(nil)
//        }
//        self.children = childCopy
//    }

//    /// Return the next child trie after the specified index or nil if none
//    private func childAfter(_ index: Int) -> Trie<T>? {
//        guard let children = self.children else {
//            return nil
//        }
//        let look = index + 1
//        if look < children.count {
//            for t in children[(index+1)...] {
//                if let trie = t {
//                    return trie
//                }
//            }
//        }
//        if let parent = self.parent {
//            return parent.childAfter(self.indexInParent)
//        }
//        return nil
//    }
}

