//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


import Foundation

extension Trie : CustomDebugStringConvertible {
    public var debugDescription: String {
        let v = ( nil == self.value) ? "" : "\(self.value!)"
        return "\(self.pathString):\(v)"
    }
    
    public var path: [Int] {
        if let parent = self.parent {
            var path = parent.path
            path.append(self.indexInParent)
            return path
        }
        return [Int]()
    }
    
    public var pathString: String {
        var str = ""
        if let parent = self.parent {
            str = parent.pathString
            if !str.isEmpty {
                str.append(",")
            }
            str.append(String(self.indexInParent))
            if let compact = self.compact, !compact.isEmpty {
                str.append(String(describing: compact))
            }
        }
        return str
    }
    
    public var compactionPath: [Int] {
        if let compact = self.compact {
            return compact
        }
        return [Int]()
    }
    
    public var tree: String {
        var string = ""
        return self.tree(indent: "", string: &string)
    }
    
    public final var depth: Int {
        if let parent = self.parent {
            return parent.depth
        }
        return 0;
    }
    
    @discardableResult private func tree(indent: String, string: inout String) -> String {
        string.append(indent)
        string.append(self.debugDescription)
        string.append("\n")
        var child = self.firstChild
        while case let ch? = child {
            ch.tree(indent: indent + " ", string: &string)
            child = ch.nextSibling
        }
        
//        if let children = self.children {
//            for t in children {
//                if let child = t {
//                    child.tree(indent: indent + " ", string: &string)
//                }
//            }
//        }
        return string
    }
    
    
}
