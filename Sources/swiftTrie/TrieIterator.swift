//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

public class TrieIterator<T>: IteratorProtocol {

    public typealias Element = Trie<T>
    private var current: Trie<T>? = nil
    private var start: Trie<T>?

    init(_ start: Trie<T>) {
        self.start = start
        self.current = self.start
    }

    public func next() -> Trie<T>? {
        guard let start = self.start, let current = self.current else {
            return nil
        }
        self.current = current.nextTrie()
        if start === self.current {
            self.current = nil
            self.start = nil
        }
        return self.current
    }
}
