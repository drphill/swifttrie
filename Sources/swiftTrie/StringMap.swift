//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import Foundation

/// A map that stores objects against keys of type string based upon a Trie.  The objects can be enumerated in order of their
/// string keys.
public class StringMap<T>: Trie<T> {
    /// store the value against the string
    public func put(_ string: String, value: T) {
        self.putTrie(string.pathIterator).value = value
    }
    
    /// return the value stored against the string, nil otherwise
    public func get(_ string: String) -> T? {
        return self.getTrie(string.pathIterator)?.value
    }
    
    /// remove value stored aggainst string specified (replace with nil), return previous value if any
    @discardableResult public func remove(_ string: String) -> T? {
        if let trie = self.getTrie(string.pathIterator) {
            let oldValue = trie.value
            trie.value = nil
            return oldValue
        }
        return nil
    }
    
    /// remove all values stored against strings beginning with the specified string
    public func removeForStringsBeginningWith(_ string: String) {
        self.getTrie(string.pathIterator)?.remove()
    }

}
