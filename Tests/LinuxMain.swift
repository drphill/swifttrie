import XCTest

import swiftTrieTests

var tests = [XCTestCaseEntry]()
tests += swiftTrieTests.allTests()
XCTMain(tests)