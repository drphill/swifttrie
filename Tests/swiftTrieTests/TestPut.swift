//   Copyright 2018 Dr Phill van Leersum
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.//

import XCTest
import swiftTrie

class TestPut: XCTestCase {
    
    func testSimplePutGet() {
        let trieMap = StringMap<String>()
        trieMap.put("1234", value: "1234")
        
        var got = trieMap.get("1234")
        XCTAssertNotNil(got)
        XCTAssertEqual("1234", got)
        
        print("\n\n")
        trieMap.put("1235", value: "1235")
        got = trieMap.get("1235")
        XCTAssertNotNil(got)
        XCTAssertEqual("1235", got)
        
        print(trieMap.tree)
        print("\n\n")
        trieMap.put("199", value: "199")
        got = trieMap.get("199")
        XCTAssertNotNil(got)
        XCTAssertEqual("199", got)
        print(trieMap.tree)

        trieMap.put("1245", value: "1245")
        got = trieMap.get("1245")
        XCTAssertNotNil(got)
        XCTAssertEqual("1245", got)
        
        for t in trieMap {
            print(t)
        }
    }
    
    func testExample() {
        let trie = Trie<String>()
        var subtrie = trie.putTrie(IntegerPathIterator([1,2,3,1]))
        subtrie.value = "test"
        //print(trie.tree)
        
        //print(subtrie.debugDescription)
        subtrie = trie.putTrie(IntegerPathIterator([1,2,3,10]))
        subtrie.value = "test2"
        
        subtrie = trie.putTrie(IntegerPathIterator([1,2,2,1]))
        subtrie.value = "testx"
        subtrie = trie.putTrie(IntegerPathIterator([1,2,2,2]))
        subtrie.value = "testy"
        subtrie = trie.putTrie(IntegerPathIterator([1,2,2,3]))
        subtrie.value = "testz"
        
        
        print(trie.tree)
        
        if let subtrie = trie.getTrie(IntegerPathIterator([1,2,3,1])) {
            XCTAssert(subtrie.value == "test")
        } else {
            XCTFail("not found")
        }
        if let subtrie = trie.getTrie(IntegerPathIterator([1,2,3,10])) {
            XCTAssert(subtrie.value == "test2")
        } else {
            XCTFail("not found")
        }
        
        for t in trie {
            print(t)
        }
    }

}
